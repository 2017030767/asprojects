package com.example.searchviewcustom;

public class ItemData {
    private String textNombre;
    private String textTelefono;
    private Integer imageId;

    public ItemData(String textNombre, String textTelefono, Integer imageId) {
        this.textNombre = textNombre;
        this.textTelefono = textTelefono;
        this.imageId = imageId;
    }

    public String getTextNombre() {
        return textNombre;
    }

    public String getTextTelefono() {
        return textTelefono;
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setTextNombre(String textNombre) {
        this.textNombre = textNombre;
    }

    public void setTextTelefono(String textTelefono) {
        this.textTelefono = textTelefono;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }
}
