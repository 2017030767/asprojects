package com.example.searchviewcustom;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ListViewAdapter extends ArrayAdapter<ItemData> implements Filterable {
    private int groupId;
    private Activity Context;
    private ArrayList<ItemData> list;
    private ArrayList<ItemData> filterList;
    private CustomFilter filter;
    private LayoutInflater inflater;

    public ListViewAdapter(Activity Context,int groupId,ArrayList<ItemData> list){
        super(Context,groupId,list);
        this.list = list;
        this.inflater = (LayoutInflater) Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupId = groupId;
        this.filterList = list;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        View itemView = inflater.inflate(groupId,parent,false);
        ImageView imagen = (ImageView) itemView.findViewById(R.id.imgPersona);
        imagen.setImageResource(list.get(position).getImageId());
        TextView textNombre = (TextView) itemView.findViewById(R.id.lblNombre);
        textNombre.setText(list.get(position).getTextNombre());
        TextView textTelefono = (TextView) itemView.findViewById(R.id.lblTelefono);
        textTelefono.setText(list.get(position).getTextTelefono());
        return itemView;
    }

    @Override
    public int getCount(){
        return this.list.size();
    }

    @Override
    public ItemData getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return list.indexOf(getItem(pos));
    }

    @Override
    public Filter getFilter(){
        if(filter == null){
            filter = new CustomFilter();
        }
        return filter;
    }

    private class CustomFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length()>0){
                constraint = constraint.toString().toUpperCase();
                ArrayList<ItemData> filters = new ArrayList<ItemData>();

                for(ItemData itemList: filterList){
                    if(itemList.getTextNombre().toUpperCase().contains(constraint)){
                        filters.add(itemList);
                    }
                }
                results.count = filters.size();
                results.values = filters;
            }else{
                results.count = filterList.size();
                results.values = filterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            list = (ArrayList<ItemData>) results.values;
            notifyDataSetChanged();
        }
    }

}
