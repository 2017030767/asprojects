package com.example.searchviewcustom;

import androidx.appcompat.app.AppCompatActivity;


import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    private ListView listView;
    private SearchView srcLista;
    private ListViewAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ArrayList<ItemData> list = new ArrayList<>();
        list.add(new ItemData(getString(R.string.nom_1),getString(R.string.tel_1), R.drawable.aaa));
        list.add(new ItemData(getString(R.string.nom_2),getString(R.string.tel_2), R.drawable.bbb));
        list.add(new ItemData(getString(R.string.nom_3),getString(R.string.tel_3), R.drawable.ccc));
        list.add(new ItemData(getString(R.string.nom_4),getString(R.string.tel_4), R.drawable.ddd));
        list.add(new ItemData(getString(R.string.nom_5),getString(R.string.tel_5), R.drawable.eee));
        list.add(new ItemData(getString(R.string.nom_6),getString(R.string.tel_6), R.drawable.fff));
        list.add(new ItemData(getString(R.string.nom_7),getString(R.string.tel_7), R.drawable.ggg));
        list.add(new ItemData(getString(R.string.nom_8),getString(R.string.tel_8), R.drawable.hhh));
        list.add(new ItemData(getString(R.string.nom_9),getString(R.string.tel_9), R.drawable.iii));
        list.add(new ItemData(getString(R.string.nom_10),getString(R.string.tel_10), R.drawable.jjj));
        list.add(new ItemData(getString(R.string.nom_11),getString(R.string.tel_11), R.drawable.kkk));
        list.add(new ItemData(getString(R.string.nom_12),getString(R.string.tel_12), R.drawable.lll));

        this.listView = (ListView) findViewById(R.id.lvPersonas);
        this.srcLista = (SearchView) findViewById(R.id.menu_search);

        this.adapter = new ListViewAdapter(MainActivity.this,R.layout.listview_layout,list);
        this.listView.setAdapter(adapter);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(parent.getContext(),"Ha seleccionado " + list.get(position).getTextNombre(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.searchview,menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {


                adapter.getFilter().filter(s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
}

