package com.example.cotizacion;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


public class CotizacionActivity extends AppCompatActivity {
    private Cotizacion cotizacion;
    private TextView lblFolio;
    private TextView lblNombre;
    private EditText txtDescription;
    private EditText txtValor;
    private EditText txtPorEnganche;
    private RadioGroup rgrpPlazos;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    private TextView lblPagoMensual;
    private TextView lblEnganche;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotizacion);

        lblFolio = (TextView) findViewById(R.id.lblFolio);
        lblNombre = (TextView) findViewById(R.id.lblNombre);

        txtDescription = (EditText) findViewById(R.id.txtDescripcion);
        txtValor = (EditText) findViewById(R.id.txtValor);
        txtPorEnganche = (EditText) findViewById(R.id.txtPorEnganche);

        rgrpPlazos = (RadioGroup) findViewById(R.id.rgrpPlazos);

        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);

        lblPagoMensual = (TextView) findViewById(R.id.lblPagoMensual);
        lblEnganche = (TextView) findViewById(R.id.lblEnganche);

        Bundle datos = getIntent().getExtras();

        String nombre = "Cliente: " + datos.getString("cliente");
        lblNombre.setText(nombre);

        cotizacion = (Cotizacion) datos.getSerializable("cotizacion");

        String folio = "Folio: " + String.valueOf(cotizacion.getFolio());
        lblFolio.setText(folio);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(txtDescription.getText()) ||
                        TextUtils.isEmpty(txtValor.getText())){
                    Toast.makeText(CotizacionActivity.this,"Favor de llenar los campos", Toast.LENGTH_SHORT).show();
                }else{
                    cotizacion.setDescripcion(txtDescription.getText().toString());
                    cotizacion.setPorEnganche(Float.parseFloat(txtPorEnganche.getText().toString()));
                    cotizacion.setValorAuto(Float.parseFloat(txtValor.getText().toString()));

                    switch (rgrpPlazos.getCheckedRadioButtonId()){
                        case R.id.rbtn12:
                            cotizacion.setPlazos(12);
                            break;
                        case R.id.rbtn18:
                            cotizacion.setPlazos(18);
                            break;
                        case R.id.rbtn24:
                            cotizacion.setPlazos(24);
                            break;
                        case R.id.rbtn36:
                            cotizacion.setPlazos(36);
                            break;
                        default:
                            Toast.makeText(CotizacionActivity.this,"Error en los plazos", Toast.LENGTH_LONG).show();
                            break;
                    }

                    String pagoMes = getString(R.string.pago_mes) + " " + String.valueOf(cotizacion.calcularPagoMensual());
                    lblPagoMensual.setText(pagoMes);

                    String enganche = getString(R.string.enganche) + " " + String.valueOf(cotizacion.calcularEnganche());
                    lblEnganche.setText(enganche);


                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtDescription.setText("");
                txtValor.setText("");
                txtPorEnganche.setText("");

                lblPagoMensual.setText(R.string.pago_mes);
                lblEnganche.setText(R.string.enganche);
                rgrpPlazos.check(R.id.rbtn12);
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

    }
}
