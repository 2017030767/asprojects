package com.example.preexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class ConvertidorActivity extends AppCompatActivity {
    private TextView txtUsuario;
    private EditText txtTemperatura;
    private RadioGroup rgrpConversion;
    private TextView txtResultado;
    private Button btnConvertir;
    private Button btnPurificar;
    private Button btnVolver;
    private double resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convertidor);

        txtUsuario = (TextView) findViewById(R.id.txtUsuario);
        txtTemperatura = (EditText) findViewById(R.id.txtTemperatura);
        rgrpConversion = (RadioGroup) findViewById(R.id.rgrpConversion);
        txtResultado = (TextView) findViewById(R.id.txtResultado);

        btnConvertir = (Button) findViewById(R.id.btnConvertir);
        btnPurificar = (Button) findViewById(R.id.btnPurificar);
        btnVolver = (Button) findViewById(R.id.btnVolver);

        Bundle datos = getIntent().getExtras();

        txtUsuario.setText(datos.getString("nombre"));

        btnConvertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(txtTemperatura.getText())){
                    Toast.makeText(ConvertidorActivity.this, "Falto ingresar una cantidad", Toast.LENGTH_SHORT).show();
                }else{
                    float cantidad = Float.parseFloat(txtTemperatura.getText().toString());
                    switch (rgrpConversion.getCheckedRadioButtonId()){
                        case R.id.rbtnCelsius:
                            //1.8 equivale a 9/5
                            resultado = (1.8 * cantidad) + 32;
                            break;
                        case R.id.rbtnFahr:
                            //dividirlo entre 1.8 equivale a multiplicarlo por 5/9
                            resultado = (cantidad - 32)/1.8;
                            break;
                        default:
                            Toast.makeText(ConvertidorActivity.this,"Error en los radio button", Toast.LENGTH_LONG).show();

                            break;
                    }
                    resultado = (double)Math.round(resultado * 100d) / 100d;
                    txtResultado.setText(String.valueOf(resultado));
                }
            }
        });

        btnPurificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtTemperatura.setText("");
                txtResultado.setText("");
                rgrpConversion.check(R.id.rbtnCelsius);
            }
        });

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
