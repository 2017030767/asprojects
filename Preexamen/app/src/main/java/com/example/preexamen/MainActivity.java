package com.example.preexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtNombre;
    private Button btnIMC;
    private Button btnConvertidor;
    private Button btnTerminar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNombre = (EditText) findViewById(R.id.txtNombre);
        btnIMC = (Button) findViewById(R.id.btnIMC);
        btnConvertidor = (Button) findViewById(R.id.btnConvertidor);
        btnTerminar = (Button) findViewById(R.id.btnTerminar);

        btnIMC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombre = txtNombre.getText().toString();
                if(nombre.matches("")){
                    Toast.makeText(MainActivity.this, "Falto capturar nombre", Toast.LENGTH_SHORT).show();
                }else{
                    Intent imcIntent = new Intent(MainActivity.this, IMCActivity.class);

                    //Enviar el dato de nombre
                    imcIntent.putExtra("nombre", nombre);

                    startActivity(imcIntent);

                }

            }
        });
        btnConvertidor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombre = txtNombre.getText().toString();
                if(nombre.matches("")){
                    Toast.makeText(MainActivity.this, "Falto capturar nombre", Toast.LENGTH_SHORT).show();
                }else{
                    Intent converterIntent = new Intent(MainActivity.this, ConvertidorActivity.class);

                    //Enviar el dato de nombre
                    converterIntent.putExtra("nombre", nombre);

                    startActivity(converterIntent);

                }

            }
        });

        btnTerminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
