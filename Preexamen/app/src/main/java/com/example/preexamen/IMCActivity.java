package com.example.preexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class IMCActivity extends AppCompatActivity {
    private TextView txtPersona;
    private EditText txtAltura;
    private EditText txtPeso;
    private TextView txtIMC;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imc);

        txtPersona = (TextView) findViewById(R.id.txtPersona);
        txtAltura = (EditText) findViewById(R.id.txtAltura);
        txtPeso = (EditText) findViewById(R.id.txtPeso);
        txtIMC = (TextView) findViewById(R.id.txtIMC);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);

        Bundle datos = getIntent().getExtras();

        txtPersona.setText(datos.getString("nombre"));


        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(txtAltura.getText()) || TextUtils.isEmpty(txtPeso.getText())){
                    Toast.makeText(IMCActivity.this, "Por favor llene los campos", Toast.LENGTH_SHORT).show();
                }else{
                    float altura = Float.parseFloat(txtAltura.getText().toString());
                    float peso = Float.parseFloat(txtPeso.getText().toString());

                    double imc = peso / (altura * altura);

                    imc = (double)Math.round(imc * 100d) / 100d;
                    txtIMC.setText(String.valueOf(imc));
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtAltura.setText("");
                txtPeso.setText("");
                txtIMC.setText("");
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
