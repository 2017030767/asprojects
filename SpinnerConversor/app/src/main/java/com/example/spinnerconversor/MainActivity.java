package com.example.spinnerconversor;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Spinner spnDivisas;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnCerrar;
    private EditText txtCantidad;
    private TextView lblTotal;
    private String divisa = "";
    private double conversion = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        txtCantidad = (EditText) findViewById(R.id.txtCantidad);
        lblTotal =(TextView) findViewById(R.id.lblTotal);


        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txtCantidad.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this,"Favor de ingresar una cantidad valida",Toast.LENGTH_SHORT).show();
                }
                else{
                    Double cantidad = Double.parseDouble(txtCantidad.getText().toString());
                    cantidad *= conversion;
                    lblTotal.setText("Total $" + cantidad + " " + divisa);
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lblTotal.setText("Total");
                txtCantidad.setText("");
                spnDivisas.setSelection(0);
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        spnDivisas = (Spinner) findViewById(R.id.spnDivisas);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,
                android.R.layout.simple_expandable_list_item_1,
                getResources().getStringArray(R.array.divisas));
        spnDivisas.setAdapter(adapter);
        spnDivisas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                divisa = parent.getItemAtPosition(position).toString();
                //long idx = parent.getItemIdAtPosition(position);
                switch (position){
                    case 0:
                        conversion = 0.053568;
                        break;
                    case 1:
                        conversion = 0.048294;
                        break;
                    case 2:
                        conversion = 0.069990;
                        break;
                    case 3:
                        conversion = 0.041160;
                        break;
                    default:
                        Toast.makeText(MainActivity.this, "Algo salio mal", Toast.LENGTH_LONG).show();
                        break;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
}
